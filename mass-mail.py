#!/usr/bin/env python3

"""
This module sends mass-mailing, for example if you want to invite
your whole Hackerspace to a meting.
"""
import argparse
import email
import email.mime.text
import smtplib
import time

parser = argparse.ArgumentParser()
parser.add_argument("addressfile", help="File with TO: addresses. One per line")
parser.add_argument("mailtext", help="File with the text to send.")
parser.add_argument("fromaddr", help="From Address @stratum0.org. Will be used for login!")
parser.add_argument("subject", help="The subject of this e-mail")
parser.add_argument("--ccaddress", help="An address to CC in all Mails", default=None)
args = parser.parse_args()

with open(args.addressfile) as fh:
    to_addr = fh.read().split("\n")
print("Loadrd {} addresses".format(len(to_addr)))

with open(args.mailtext) as fh:
    mail_text = fh.read()
print("Mailtext loaded")

passwd="FIXME"

if passwd == "FIXME":
    print("Please Enter password for your mail account!")
    exit(1)

for addr in to_addr:
    addr = addr.strip()
    if len(addr) == 0:
        continue
    print("Sending to: {} ".format(addr))

    to_addrs = [addr]
    msg =email.mime.text.MIMEText(mail_text)
    msg["Subject"] = args.subject
    msg["From"] = args.fromaddr
    msg["To"] = addr
    if args.ccaddress:
        msg["CC"] = args.ccaddress
        to_addrs.append(args.ccaddress)
    msgid = email.utils.make_msgid()
    msg["Message-ID"] = msgid
    msg["Date"] = email.utils.formatdate(localtime=True)

    try:
        s = smtplib.SMTP("mauron.stratum0.org", port=587)
        s.connect("mauron.stratum0.org", port=587)
        s.ehlo()
        s.starttls()
        s.ehlo()
        s.login(args.fromaddr, passwd)
        s.sendmail(args.fromaddr, to_addrs, msg.as_string())
        s.quit()
    except smtplib.SMTPRecipientsRefused as exep:
        print("-> Failed: {}".format(str(exep)))
    time.sleep(2)
